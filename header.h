#ifndef _PWM_H_
#define _PWM_H_

void pwm_increase(int channel);
void pwm_decrease(int channel);
void pwm_set(int channel,int output);
void arduino_pin_init(void);

#endif
/**********************************************************************/

/**********************************************************************/
#ifndef PID_H_
#define PID_H_

#include <stdint.h>

typedef enum PID_MODE {
	/* Use PID_MODE_DERIVATIV_NONE for a PI controller (vs PID) */
	PID_MODE_DERIVATIV_NONE = 0,
	/* PID_MODE_DERIVATIV_CALC calculates discrete derivative from previous error,
	 * val_dot in pid_calculate() will be ignored */
	PID_MODE_DERIVATIV_CALC,
	/* PID_MODE_DERIVATIV_CALC_NO_SP calculates discrete derivative from previous value,
	 * setpoint derivative will be ignored, val_dot in pid_calculate() will be ignored */
	PID_MODE_DERIVATIV_CALC_NO_SP,
	/* Use PID_MODE_DERIVATIV_SET if you have the derivative already (Gyros, Kalman) */
	PID_MODE_DERIVATIV_SET
} pid_mode_t;

typedef struct {
	pid_mode_t mode;
	float dt_min;
	float kp;
	float ki;
	float kd;
	float integral;
	float integral_limit;
	float output_limit;
	float error_previous;
	float last_output;
} PID_t;

void pid_init(PID_t *pid, pid_mode_t mode, float dt_min);
int pid_set_parameters(PID_t *pid, float kp, float ki, float kd, float integral_limit, float output_limit);
float pid_calculate(PID_t *pid, float sp, float val, float val_dot, float dt);
void pid_reset_integral(PID_t *pid);
float yaw_pid_calculate(PID_t *pid, float sp, float val, float val_dot, float dt);

#endif /* PID_H_ */
/**********************************************************************/

/**********************************************************************/
#ifndef _pid_calibrate_H_
#define _pid_calibrate_H_

#if defined(BOARD_IOTDK) || defined(BOARD_EMSDP)
#define MPU9250_IIC_ID DFSS_IIC_0_ID
#else
#define MPU9250_IIC_ID 0
#endif

typedef struct {
	int pid_ret;				// 1: set unsuccessful; 0: set successful
	PID_t pid_p;
	PID_t pid_r;
	PID_t pid_y;
	MPU9250_DATA mpu9250_data;
	float kp;
	float ki;
	float kd;
	float integral_limit;
	float output_limit;
	
	float pitch_pid;
	float roll_pid;
	float yaw_pid;

	float speed_pitch;
	float speed_roll;
	float speed_yaw;

	int pin_motor1;//counterclockwise
	int pin_motor2;//clockwise
	int pin_motor3;//clockwise
	int pin_motor4;//counterclockwise

	float speed_motor1;
	float speed_motor2;
	float speed_motor3;
	float speed_motor4;

	float goal_pitch_error;//pitch starting point
	float goal_roll_error;

	float goal_pitch;
	float goal_roll;
	float goal_yaw;
    
	int calibrate_max_speed;

	int motor_initial_speed;

	float pre_error;

	int i_1[10];
	int t;
} loiter_t;
void loiter(loiter_t *loit, int i,int initial_speed);
void loiter_set_parameter(loiter_t *loit, float goal_pitch_in, float goal_roll_in, float goal_yaw_in);
#endif
/**********************************************************************/

/**********************************************************************/
/*
#ifndef YAW_ERROR_H_
#define YAW_ERROR_H_
#include <stdint.h>

void yaw_error_count_start(void);
//uint64_t real_timer_ms(void);
//uint64_t real_timer_s(int TIMER);
void yaw_calibrate(float *error, uint64_t *time, uint64_t *pre);

#endif
*/
/**********************************************************************/
