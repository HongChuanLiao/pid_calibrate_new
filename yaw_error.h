#ifndef YAW_ERROR_H_
#define YAW_ERROR_H_
#include <stdint.h>

void yaw_error_count_start(void);
//uint64_t real_timer_ms(void);
//uint64_t real_timer_s(int TIMER);
void yaw_calibrate(float *error, uint64_t *time, uint64_t *pre);

#endif
